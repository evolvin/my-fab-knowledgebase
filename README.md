# my-fab-knowledgebase
[TOC]

## My basic setup
These are notes I captured from a combination of my Good Docs Projects Gitlab training and what I've learned along the 
Here are my steps to get started with Gitlab.
**Note:** This assumes I have a repo already in Gitlab.
1. Clone my remo using `git clone my_git_repo_url`
2. Enter `cd my_git_repo_name` **Note:** I don't need to have `.git` at the end of the name.
3. Enter `git remote -v` to see what repos for fetch and push your "local directory" is connected to.
4. In my VS Code terminal, enter `git checkout -b branch_name`
5. Go to the **Explorer** in VS Code. Here, do you go to **File>Open Folder**?
6. Make changes to the content in **Explorer**.
7. Save the changes.
8. Enter `git status` to view the changes. You should see the "modified" statement in red.
9. Enter `git add .` to stage the changes.
10. Enter `git status` again to view the changes. You should see the "modified" statement in green.
11. Enter `git commit -m "message"`
12. Enter `git push origin name_of_branch`
13. Go to Gitlab to manage the merge.
   
## Review Changes in Gitlab and Merge

1. Go to your repo.
2. Go to **Overview**
3. Select **Create Merge Request**. A new window appears.
4. Confirm your **Title** and enter comments.
5. Assign the task of merging to myself.
6. Assign reviewers if needed.
7. Select **Create Merge Request**. 
8. Go to **Changes**.
9. Review the changes.
10. Go back to **Overview** and click **Merge**

## After Merge, Pulldown Repo

1. Enter `git checkout main`
2. Enter `git pull origin main`
